package org.jasig.cas.client.proxy;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Martin Snyman <martin_snyman@campuseai.org>
 * @since 1.0.0
 */
public class HazelcastBackedProxyGrantingTicketStorageImpl extends AbstractEncryptedProxyGrantingTicketStorageImpl {

    public static final String HAZELCAST_CACHE_MAP_NAME = "org.jasig.cas.client.proxy.HazelcastBackedProxyGrantingTicketStorageImpl.cache";
    public static final String HAZELCAST_INSTANCE = "org.jasig.cas.client.proxy.HazelcastBackedProxyGrantingTicketStorageImpl.PGT_STORAGE";
    protected static final Logger log = LoggerFactory.getLogger(HazelcastBackedProxyGrantingTicketStorageImpl.class);
    private final IMap<String, String> cache;

    /**
     *
     */
    public HazelcastBackedProxyGrantingTicketStorageImpl() {
        HazelcastInstance hazelcastInstanceByName = Hazelcast.getHazelcastInstanceByName(HAZELCAST_INSTANCE);
        if (hazelcastInstanceByName == null) {
            Config config = new Config();
            config.setInstanceName(HAZELCAST_INSTANCE);
            hazelcastInstanceByName = Hazelcast.newHazelcastInstance(config);
        }
        this.cache = hazelcastInstanceByName.getMap(HAZELCAST_CACHE_MAP_NAME);
        log.debug("Initialize with default cache named:" + this.cache.getName());
    }

    /**
     *
     */
    public HazelcastBackedProxyGrantingTicketStorageImpl(final IMap<String, String> cache) {
        super();
        this.cache = cache;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.jasig.cas.client.proxy.ProxyGrantingTicketStorage#cleanUp()
     */
    public void cleanUp() {
        // no op
    }

    /*
     * (non-Javadoc)
     *
     * @see org.jasig.cas.client.proxy.AbstractEncryptedProxyGrantingTicketStorageImpl#saveInternal(java.lang.String,
     * java.lang.String)
     */
    @Override
    protected void saveInternal(final String proxyGrantingTicketIou, final String proxyGrantingTicket) {
        log.debug(String.format("Saving pgtiou/pgt [%s]/[%s]", proxyGrantingTicketIou, proxyGrantingTicket));
        cache.put(proxyGrantingTicketIou, proxyGrantingTicket);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.jasig.cas.client.proxy.AbstractEncryptedProxyGrantingTicketStorageImpl#retrieveInternal(java.lang.String)
     */
    @Override
    protected String retrieveInternal(final String proxyGrantingTicketIou) {
        if (proxyGrantingTicketIou == null) {
            if (log.isDebugEnabled()) log.debug("The pgtiou was provided as null value");
            return null;
        }
        if (log.isDebugEnabled()) log.debug(String.format("Searching for pgtiou [%s]", proxyGrantingTicketIou));
        String result = proxyGrantingTicketIou == null ? null : cache.get(proxyGrantingTicketIou);
        if (log.isDebugEnabled()) log.debug(String.format("Found pgt [%s]", result));
        //------ We've seen situations where the multicast did not happen fast enough -- so lets give it a second or two and then retry
        if (result == null) {
            if (log.isDebugEnabled())
                log.debug(String.format("Did not find pgt on 1st try using pgtiou [%s], pausing before retry", proxyGrantingTicketIou));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                // no op
            } finally {
                result = proxyGrantingTicketIou == null ? null : cache.get(proxyGrantingTicketIou);
                if (log.isDebugEnabled()) log.debug(String.format("Found pgt on 2nd try [%s]", result));
            }
        }
        return result;
    }
}
