package org.jasig.cas.client.proxy;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Martin Snyman <martin_snyman@campuseai.org>
 * @since 1.0.0
 */
public class HazelcastBackedProxyGrantingTicketStorageTests {

    @Test
    public void testTicketRetrieval() {
        Config config = new Config();
        config.setInstanceName(HazelcastBackedProxyGrantingTicketStorageImpl.HAZELCAST_INSTANCE);
        Hazelcast.newHazelcastInstance(config);

        final HazelcastInstance hazelcastInstanceByName = Hazelcast
                .getHazelcastInstanceByName(HazelcastBackedProxyGrantingTicketStorageImpl.HAZELCAST_INSTANCE);
        assertNotNull(hazelcastInstanceByName);

        final IMap<String, String> hcache = hazelcastInstanceByName
                .getMap(HazelcastBackedProxyGrantingTicketStorageImpl.HAZELCAST_CACHE_MAP_NAME);
        assertEquals(0, hcache.size());

        final HazelcastBackedProxyGrantingTicketStorageImpl cache = new HazelcastBackedProxyGrantingTicketStorageImpl();
        assertNull(cache.retrieve(null));
        assertNull(cache.retrieve("foobar"));

        cache.save("proxyGrantingTicketIou", "proxyGrantingTicket");
        assertEquals("proxyGrantingTicket", cache.retrieve("proxyGrantingTicketIou"));
        assertTrue("proxyGrantingTicket".equals(hcache.get("proxyGrantingTicketIou")));

    }
}
