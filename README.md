# cas-client-support-distributed-hazelcast


Support for a Hazelcast backed Proxy Granting Ticket Storage.


## History

* 2013-05-10 : Updated to Hazelcast 2.5


## JMX

It may be useful to enable the JMX components using `-Dhazelcast.jmx=true`
